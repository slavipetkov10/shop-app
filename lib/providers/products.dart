import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '../models/http_exception.dart';
import './product.dart';

// With mixin we merge properties and methods in this existing class,
// But do not turn the class into an instance of the inherited class
// ChangeNotifier gives utility methods such as     notifyListeners();
class Products with ChangeNotifier {
  List<Product> _items = [
    // Product(
    //   id: 'p1',
    //   title: 'Red Shirt',
    //   description: 'A red shirt - it is pretty red!',
    //   price: 29.99,
    //   imageUrl:
    //       'https://cdn.pixabay.com/photo/2016/10/02/22/17/red-t-shirt-1710578_1280.jpg',
    // ),
    // Product(
    //   id: 'p2',
    //   title: 'Trousers',
    //   description: 'A nice pair of trousers.',
    //   price: 59.99,
    //   imageUrl:
    //       'https://image.shutterstock.com/image-photo/light-grey-formal-mens-trousers-600w-1096296503.jpg',
    // ),
    // Product(
    //   id: 'p3',
    //   title: 'Yellow Scarf',
    //   description: 'Warm and cozy - exactly what you need for the winter.',
    //   price: 19.99,
    //   imageUrl:
    //       'https://live.staticflickr.com/4043/4438260868_cc79b3369d_z.jpg',
    // ),
    // Product(
    //   id: 'p4',
    //   title: 'A Pan',
    //   description: 'Prepare any meal you want.',
    //   price: 49.99,
    //   imageUrl:
    //       'https://image.shutterstock.com/image-photo/flying-pan-isolated-on-white-260nw-1404142046.jpg',
    // ),
  ];

  // var _showFavoritesOnly = false;
  final String authToken;
  // The id of the user who is logged in
  final String userId;

  Products(this.authToken, this.userId, this._items);

  // Returns a copy of the elements in _items
  List<Product> get items {
    // if(_showFavoritesOnly) {
    //   return _items.where((prodItem) => prodItem.isFavorite).toList();
    // }
    return [..._items];
  }

  List<Product> get favoriteItems {
    return _items.where((prodItem) => prodItem.isFavorite).toList();
  }

  Product findById(String id) {
    return _items.firstWhere((prod) => prod.id == id);
  }

  // void showFavoritesOnly() {
  //   _showFavoritesOnly = true;
  //   // Rebuilds the parts that are interested in the Products data
  //   notifyListeners();
  // }
  //
  // void showAll() {
  //   _showFavoritesOnly = false;
  //   notifyListeners();
  // }

  // Optional, positional argument
  Future<void> fetchAndSetProducts([bool filterByUser = false]) async {
    // Firebase wants the token for authentication at the end of the url in
    // a ?auth=token
    // Filter the products by a user on the server by the url
    // &orderBy="creatorId"&equalTo="$userId"
    // In firebase rules add the field creatorId on which we want to filter
    // "products": {
    //       ".indexOn": ["creatorId"]
    //     }
    final filterString = filterByUser ? 'orderBy="creatorId"&equalTo="$userId"' : '';
    var url =
        'https://flutter-update-ed6d8-default-rtdb.firebaseio.com/products.json?auth=$authToken&$filterString';
    try {
      final response = await http.get(url);
      // The values are also maps
      final extractedData = json.decode(response.body) as Map<String, dynamic>;
      if(extractedData == null){
        return;
      }
      url =
          'https://flutter-update-ed6d8-default-rtdb.firebaseio.com/userFavorites/$userId.json?auth=$authToken';
      final favoriteResponse = await http.get(url);
      // Map with a key, which is the product id and a value which is false
      // or true
      final favoriteData = json.decode(favoriteResponse.body);
      final List<Product> loadedProducts = [];
      // Execute a function on every id in the map
      extractedData.forEach((prodId, prodData) {
        // Convert the product data into product objects
        loadedProducts.add(Product(
          id: prodId,
          title: prodData['title'],
          description: prodData['description'],
          price: prodData['price'],
          // if find no entry for favoriteData[prodId] the same as if it
          // is null return false
          isFavorite: favoriteData == null ? false : favoriteData[prodId] ?? false,
          imageUrl: prodData['imageUrl'],
        ));
      });
      _items = loadedProducts;
      notifyListeners();
    } catch (error) {
      throw (error);
    }
  }

  // This class will be used by the provider package, which uses
  // Inherited widgets behind, therefore it established a connection between
  // this class and the widget that are interested of this class
  // We want to let the widgets that use this class , when there is a change
  // in the list, then they will be rebuild and a copy of the updated list

  // In the method we save the newProduct locally and send http request to
  // the web server
  //https://flutter-update-ed6d8-default-rtdb.firebaseio.com/
  // Return a future with no type
  // async method always return a Future
  Future<void> addProduct(Product product) async {
    // Adding an extra segment after the url creates a folder in the database
    final url =
        'https://flutter-update-ed6d8-default-rtdb.firebaseio.com/products.json?auth=$authToken';
    try {
      // with await we want this operation to finish and then move on with the code
      final response = await http.post(
        url,
        body: json.encode({
          'title': product.title,
          'description': product.description,
          'imageUrl': product.imageUrl,
          'price': product.price,
          'creatorId': userId,
        }),
      );
      // When the post() method is done this function is executed
      // This part is invisibly wrapped in a then() block
      final newProduct = Product(
        title: product.title,
        description: product.title,
        price: product.price,
        imageUrl: product.imageUrl,
        // Convert from JSON to a map with a name key
        id: json.decode(response.body)['name'],
      );
      _items.add(newProduct);
      // _items.insert(0, newProduct);// at the start of the list
      notifyListeners();
    } catch (error) {
      print(error);
      throw error;
    }
  }

  Future<void> updateProduct(String id, Product newProduct) async {
    final prodIndex = _items.indexWhere((prod) => prod.id == id);
    if (prodIndex >= 0) {
      final url =
          'https://flutter-update-ed6d8-default-rtdb.firebaseio.com/products/$id.json?auth=$authToken';
      // patch() merges the incomming data with the existing data at the
      // provided address /$id
      http.patch(url,
          body: json.encode({
            'title': newProduct.title,
            'description': newProduct.description,
            'imageUrl': newProduct.imageUrl,
            'price': newProduct.price
          }));
      _items[prodIndex] = newProduct;
      notifyListeners();
    } else {
      print('...');
    }
  }

  Future<void> deleteProduct(String id) async {
    final url =
        'https://flutter-update-ed6d8-default-rtdb.firebaseio.com/products/$id.json?auth=$authToken';
    final existingProductIndex = _items.indexWhere((prod) => prod.id == id);
    // Store a reference of the product in memory
    var existingProduct = _items[existingProductIndex];
    // Delete the product before the response has arrived
    // Remove the item from the list
    _items.removeAt(existingProductIndex);
    notifyListeners();
    // Here we are interested when the deletion failed
    // delete() throws an error greater than 400
    final response = await http.delete(url);
    if (response.statusCode >= 400) {
      //row back the removal and re-add the product
      _items.insert(existingProductIndex, existingProduct);
      notifyListeners();
      //Throw my own error
      throw HttpException('Could not delete product.');
    }
    // If there was not error while deleting the product, set it to null
    existingProduct = null;
  }
}
