import 'dart:convert';
import 'dart:async';

import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../models/http_exception.dart';

// https://firebase.google.com/docs/reference/rest/auth#section-create-email-password
// https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=[API_KEY]

// https://console.firebase.google.com/u/0/project/flutter-update-ed6d8/settings/general
// Web API key AIzaSyBfn_r58Flj_iicoW0tJVR9Qxgt2Tf5l08

// Add a mixin to be able to call notify listeners so that the parts of the UI
// that depend on Auth logic are updated when that changes
class Auth with ChangeNotifier {
  // Used to attach it to requests that reach end points, which do need
  // authentication, typically expires after 1 hour
  String _token;
  // Expiration date for the token
  DateTime _expiryDate;
  String _userId;
  Timer _authTimer;

  // If there is a token and it is not expired, then the user is authenticated
  bool get isAuth {
    return token != null;
  }

  String get token {
    if (_expiryDate != null &&
        _expiryDate.isAfter(DateTime.now()) &&
        _token != null) {
      // The user is authenticated
      return _token;
    }
    return null;
  }

  String get userId {
    return _userId;
  }

  // The method will be called only from inside the file
  Future<void> _authenticate(
      String email, String password, String urlSegment) async {
    final url =
        'https://identitytoolkit.googleapis.com/v1/accounts:$urlSegment?key=AIzaSyBfn_r58Flj_iicoW0tJVR9Qxgt2Tf5l08';
    try {
      // Send the information from the documentation Request Body Payload
      final response = await http.post(
        url,
        body: json.encode(
          {
            'email': email,
            'password': password,
            'returnSecureToken': true,
          },
        ),
      );
      // Check the response inside of try if it contains an indicator
      // that it contains an error, the indicator is called an error
      // it can be seen here : print(json.decode(response.body));
      final responseData = json.decode(response.body);
      if (responseData['error'] != null) {
        // Example error
        // I/flutter (17508): {error: {code: 400, message: EMAIL_EXISTS,
        // errors: [{message: EMAIL_EXISTS, domain: global, reason: invalid}]}}
        // Access the error key, and then in the inner map
        // EMAIL_EXISTS
        throw HttpException(responseData['error']['message']);
      }
      // When there is no error  from the firebase response payload we get
      // Response Payload idToken for the authenticated user
      _token = responseData['idToken'];
      _userId = responseData['localId'];
      _expiryDate = DateTime.now().add(
        Duration(
          seconds: int.parse(
            responseData['expiresIn'],
          ),
        ),
      );
      _autoLogout();
      notifyListeners();
      final prefs = await SharedPreferences.getInstance();
      final userData = json.encode(
        {
          'token': _token,
          'userId': _userId,
          'expiryDate': _expiryDate.toIso8601String(),
        },
      );
      prefs.setString('userData', userData);
    } catch (error) {
      throw error;
    }
  }

  Future<void> signup(String email, String password) async {
    // This is the future that wraps the http request and waits to complete
    return _authenticate(email, password, 'signUp');
  }

  Future<void> login(String email, String password) async {
    return _authenticate(email, password, 'signInWithPassword');
  }

  // returns if the login was successful
  Future<bool> tryAutoLogin() async {
    final prefs = await SharedPreferences.getInstance();
    if(!prefs.containsKey('userData')) {
      // No data is stored on the user data key
      return false;
    }
    final extractedUserData = json.decode(prefs
        .getString('userData')) as Map<String, Object>;
    final expiryDate = DateTime.parse(extractedUserData['expiryDate']);

    if(expiryDate.isBefore(DateTime.now())){
      // The token is invalid
      return false;
    }
    _token = extractedUserData['token'];
    _userId = extractedUserData['userId'];
    _expiryDate = expiryDate;
    notifyListeners();
    _autoLogout();
    return true;
  }

  Future<void> logout() async {
    _token = null;
    _userId = null;
    _expiryDate = null;
    if(_authTimer != null){
      _authTimer.cancel();
      _authTimer = null;
    }
    notifyListeners();
    // When we logout , clear the shared references
    final prefs = await SharedPreferences.getInstance();
    // prefs.remove('userData');
    prefs.clear();
  }

  void _autoLogout(){
    if(_authTimer != null){
      _authTimer.cancel();
    }
    final timeToExpiry = _expiryDate.difference(DateTime.now()).inSeconds;
    // The second argument is the function executed when the timer is done
    _authTimer = Timer(Duration(seconds: timeToExpiry), logout);
  }
}
