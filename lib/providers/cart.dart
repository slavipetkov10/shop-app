import 'package:flutter/foundation.dart';

class CartItem {
  final String id;
  final String title;

  // How much times the product was added
  final int quantity;
  final double price;

  CartItem({
    @required this.id,
    @required this.title,
    @required this.quantity,
    @required this.price,
  });
}

class Cart with ChangeNotifier {
  // The String is for Product id
  Map<String, CartItem> _items = {} ;

  Map<String, CartItem> get items {
    // Add the key, value pairs to a new map and return a copy of it
    return {..._items};
  }

  int get itemCount{
    return _items.length;
  }

  double get totalAmount{
    var total = 0.0;
    _items.forEach((key, cartItem){
      total += cartItem.price * cartItem.quantity;
    });
    return total;
  }
  // If the id is already in the cartItem, increase the quantity else add
// a new entry
  void addItem(
    String productId,
    double price,
    String title,
  ) {
    if (_items.containsKey(productId)) {
      // Change quantity
      // Finds the existing CartItem with this productItem
      _items.update(
        productId,
        (existingCartItem) => CartItem(
          id: existingCartItem.id,
          title: existingCartItem.title,
          price: existingCartItem.price,
          quantity: existingCartItem.quantity + 1,
        ),
      );
    } else {
      _items.putIfAbsent(
        productId,
        () => CartItem(
          id: DateTime.now().toString(),
          title: title,
          price: price,
          quantity: 1,
        ),
      );
    }
    // In order the listeners to be updated and the change to be displayed on UI
    notifyListeners();
  }

  void removeItem(String productId){
    _items.remove(productId);
    notifyListeners();
  }

  void removeSingleItem(String productId){
    if(!_items.containsKey(productId)){
      // This is the case for quantity == 0
      return;
    }
    if(_items[productId].quantity > 1){
      // Only reduce the quantity by 1
      _items.update(productId,
          (existingCartItem) => CartItem(
            id: existingCartItem.id,
            title: existingCartItem.title,
            price: existingCartItem.price,
            quantity: existingCartItem.quantity - 1,
          ));
    }else {
      // if quantity == 1 remove the single cart item
      _items.remove(productId);
    }
    notifyListeners();
  }

  // Placing an order clears the cart from its items
  void clear(){
    _items = {};
    notifyListeners();
  }
}
