import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

// With ChangeNotifier Products can notify listeners when they change
class Product with ChangeNotifier {
  final String id;
  final String title;
  final String description;
  final double price;
  final String imageUrl;
  bool isFavorite;

  Product({
    @required this.id,
    @required this.title,
    @required this.description,
    @required this.price,
    @required this.imageUrl,
    this.isFavorite = false,
  });

  void _setFavValue(bool newValue) {
    isFavorite = newValue;
    notifyListeners();
  }

  Future<void> toggleFavoriteStatus(String token, String userId) async {
    final oldStatus = isFavorite;
    isFavorite = !isFavorite;
    // notifyListeners is the equivalent of setState
    notifyListeners();
    final url =
        'https://flutter-update-ed6d8-default-rtdb.firebaseio.com/userFavorites/$userId/$id.json?auth=$token';
    try {
      // Keep the other data for the product on the server and just change
      // the favorite status
      // http package throws its own errors for get and post requests,
      // for patch, put, delete it does not do that
      final response = await http.put(
        url,
        body: json.encode(
          isFavorite,
        ),
      );
      if (response.statusCode >= 400) {
        _setFavValue(oldStatus);
      }
    } catch (error) {
      _setFavValue(oldStatus);
    }
  }
}
