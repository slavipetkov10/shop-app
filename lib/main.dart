import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import './screens/splash_screen.dart';
import './screens/cart_screen.dart';
import './screens/products_overview_screen.dart';
import './screens/product_detail_screen.dart';
import './providers/products.dart';
import './providers/cart.dart';
import './providers/orders.dart';
import './providers/auth.dart';
import './screens/orders_screen.dart';
import './screens/user_products_screen.dart';
import './screens/edit_product_screen.dart';
import './screens/auth_screen.dart';
import './helpers/custom_route.dart';

/*
https://thumbs-prod.si-cdn.com/c0VP9nb5bbooVSqQEjHk3q9S35c=/800x600/filters:no_upscale()/https://public-media.si-cdn.com/filer/91/91/91910c23-cae4-46f8-b7c9-e2b22b8c1710/lostbook.jpg
https://wwws.dior.com/couture/ecommerce/media/catalog/product/cache/1/zoom_image_1/3000x2000/17f82f742ffe127f42dca9de82fb58b1/Z/N/1604099792_113M222AT192_C585_E01_ZH.jpg
 */
//https://flutter-update-ed6d8-default-rtdb.firebaseio.com/
// test@test.com
// 123456
// test2@test.com
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(
          // We need Auth() data in different parts of the app
          // This provider should be first in the list so that the other providers
          // can depend on it
          value: Auth(),
        ),
        // With the provider package we provide data like objects:
        // Products here. Anywhere in the app bellow this ChangeNotifierProvider
        // we can listen on the provided values (Products) either with
        // Provider.of<Products>(context) or with
        // Consumer<Product>
        // We should get the token from Auth() and pass it to Products()
        // First provide the type of data it depends on Auth and then the data
        // that is provided Products
        ChangeNotifierProxyProvider<Auth, Products>(
          // Products (as here) should be instantiated before the widget
          // that want to setup a
          // listener to it
          // The provider provides an instance of type Products
          // In this case is better to use builder, because a class is
          // instantiated
          // The builder: receives the Auth provider from above
          update: (ctx, auth, previousProducts) => Products(
            auth.token,
            auth.userId,
            // Creates an empty list of products or with the previous items that
            // were in the list
            previousProducts == null ? [] : previousProducts.items,
          ),
        ),
        ChangeNotifierProvider.value(
          value: Cart(),
        ),
        // With this, we can listen to orders from anywhere in the application
        ChangeNotifierProxyProvider<Auth, Orders>(
          update: (ctx, auth, previousOrders) => Orders(
            auth.token,
            auth.userId,
            previousOrders == null ? [] : previousOrders.orders,
          ),
        ),
      ],
      //  builder: (ctx) => Products(),
      // All child widgets can set up a listener to this instance of the class
      // Products and when there is a change in Products the child widgets
      // will rebuild. Widgets that are bellow this MaterialApp widget can be
      // setup to listen
      // In child we can tap into all providers set above
      child: Consumer<Auth>(
        // MaterialApp rebuilds whenever Auth changes
        // builder: (auth) receives the latest auth object
        builder: (ctx, auth, _) => MaterialApp(
            title: 'MyShop',
            theme: ThemeData(
              primarySwatch: Colors.purple,
              accentColor: Colors.deepOrange,
              fontFamily: 'Lato',
              // Change the page animation transition for all routes between pages
              pageTransitionsTheme: PageTransitionsTheme(
                builders: {
                  TargetPlatform.android: CustomPageTransitionBuilder() ,
                  TargetPlatform.iOS: CustomPageTransitionBuilder(),
                },
              ),
            ),
            // If the user is authenticated show the products overview screen
            // else try the auto login, while waiting for result show the
            // SplashScreen if we are done waiting show the AuthScreen
            home: auth.isAuth
                ? ProductsOverviewScreen()
                : FutureBuilder(
                    future: auth.tryAutoLogin(),
                    builder: (ctx, authResultSnapshot) =>
                        authResultSnapshot.connectionState ==
                                ConnectionState.waiting
                            ? SplashScreen()
                            : AuthScreen(),
                  ),
            routes: {
              ProductDetailScreen.routeName: (ctx) => ProductDetailScreen(),
              CartScreen.routeName: (ctx) => CartScreen(),
              OrdersScreen.routeName: (ctx) => OrdersScreen(),
              UserProductsScreen.routeName: (ctx) => UserProductsScreen(),
              EditProductScreen.routeName: (ctx) => EditProductScreen(),
            }),
      ),
    );
  }
}
