import 'package:flutter/material.dart';

// MaterialPageRoute<T> T is the data that the page you are loading would
// resolve to once its popped off
class CustomRoute<T> extends MaterialPageRoute<T> {
  CustomRoute({
    WidgetBuilder builder,
    RouteSettings settings,
  }) : super(
          builder: builder,
          settings: settings,
        );

  // Controls how page transition is animated
  @override
  Widget buildTransitions(
    BuildContext context,
    Animation<double> animation,
    Animation<double> secondaryAnimation,
    Widget child,
  ) {
    if (settings.name == '/') {
      // There is no animation on the first page that is loaded
      //This is the first route that loads
      // return the page we are navigating to
      return child;
    }
    // If we are already in the app and just moving to a different screen
    return FadeTransition(
      opacity: animation,
      // The child that should be animated in with the animation
      child: child,
    );
  }
}

class CustomPageTransitionBuilder extends PageTransitionsBuilder {
  @override
  Widget buildTransitions<T>(
      PageRoute<T> route,
      BuildContext context,
      Animation<double> animation,
      Animation<double> secondaryAnimation,
      Widget child,
      ) {
    if (route.settings.name == '/') {
      // There is no animation on the first page that is loaded
      //This is the first route that loads
      // return the page we are navigating to
      return child;
    }
    // If we are already in the app and just moving to a different screen
    return FadeTransition(
      opacity: animation,
      // The child that should be animated in with the animation
      child: child,
    );
  }
}
