import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../screens/edit_product_screen.dart';
import '../providers/products.dart';

class UserProductItem extends StatelessWidget {
  // Get the id of the product that will be edited
  final String id;
  final String title;
  final String imageUrl;

  UserProductItem(this.id, this.title, this.imageUrl);

  @override
  Widget build(BuildContext context) {
    final scaffold = Scaffold.of(context);
    return ListTile(
        title: Text(title),
        leading: CircleAvatar(
          // An object that does the fetching of the image
          backgroundImage: NetworkImage(imageUrl),
        ),
        trailing: Container(
          width: 100,
          child: Row(children: <Widget>[
            IconButton(
              icon: Icon(Icons.edit),
              onPressed: () {
                // In arguments we set for which product we want to edit,
                // In the EditProductsScreen we will extract it in the
                // didChangeDependencies()
                Navigator.of(context)
                    .pushNamed(EditProductScreen.routeName, arguments: id);
              },
              color: Theme.of(context).primaryColor,
            ),
            IconButton(
              icon: Icon(Icons.delete),
              onPressed: () async {
                try {
                  await Provider.of<Products>(context, listen: false)
                      .deleteProduct(id);
                } catch (error) {
                  // Scaffold.of(context) does not work here, because flutter
                  // is already updating the widget tree and does not know
                  // if context refers to the same context as before
                  scaffold.showSnackBar(SnackBar(
                    content:
                        Text('Deleting failed!', textAlign: TextAlign.center),
                  ));
                }
              },
              color: Theme.of(context).errorColor,
            )
          ]),
        ));
  }
}
